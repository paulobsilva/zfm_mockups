function calculateUnreadNotifications(){
  var notificationArray = $('#notification-group > div > * > sup');
  var supArray = $('sup:not(#unread-notifications)');
  var i, tot = 0;

  for(i = 0; i < notificationArray.length; i++){
    var liSelector = "#notification-list-" + $(notificationArray[i]).data('id') + " > ul > li[data-read=false]";
    var supSelector = "sup[data-id=" + $(notificationArray[i]).data('id') + "]";

    $(supSelector).text($(liSelector).length);
  }

  for(i = 0; i < supArray.length; i++){
    tot += parseInt($(supArray[i]).text());
  }

  return $('#unread-notifications').text(tot);
}

function toggleNotificationVisibility(selector){
  console.log(selector);
    $('#notification-group').toggle(150);
    $(selector).toggle(150);
}

$(document).ready(function(){
  calculateUnreadNotifications();
  /******************************************************************************************************************************
  *******************************************************************************************************************************
                                            GROUP Notifications Handler
  *************************************************************************************************************************************************************************************************************************************************************/

  $(document).on('click', '#notification-group > div', function(evt){
    evt.stopPropagation();
    var selector = "#notification-list-" + $(this).attr('id');
    var counter = parseInt($("#" + $(this).attr('id') + " > * > sup").text());

    if(counter > 0)
      toggleNotificationVisibility(selector);
  });

  /******************************************************************************************************************************
  *******************************************************************************************************************************
                                            ITEM Notifications Handler
  *************************************************************************************************************************************************************************************************************************************************************/

  $(document).on('click', '[id=notification] > .notification-item-container', function(evt){
    evt.stopPropagation();

    $('#alert-container').show();
    $('#alert-details').text($(this).find('p').text());

    $(this).attr('data-read', true);
    calculateUnreadNotifications();
  });

  $(document).on('click', '[id=mark-read]', function(evt){
    evt.stopPropagation();
    var selector = "[id=notification][data-group=" + $(this).data('group') + "][data-id=" + $(this).data('id') + "]";
    console.log($(this).data('group'), $(this).data('id'), selector);

    $(selector).attr('data-read', true);

    calculateUnreadNotifications();
  });

  $(document).on('click', '[id=back-item]', function(evt){
    evt.stopPropagation();
    var selector = "#notification-list-" + $(this).data('group');
    toggleNotificationVisibility(selector);
  });

  /******************************************************************************************************************************
  *******************************************************************************************************************************
                                            ITEM Notifications Handler
  *************************************************************************************************************************************************************************************************************************************************************/


  $(document).on('click', '#sort-type', function(evt){
    evt.stopPropagation();
    var ulSelector = $("#notification-list-" + $(this).data('group') + " > .notifications");
    var liSelector = ulSelector.children('li#notification');
    var up = $(this).find('i:last-child').hasClass('fa-caret-up');
    var compare = $(this).data('class');

    liSelector.detach().sort(function(a, b){
      if(up){
        return $(a).find('i').hasClass(compare) - $(b).find('i').hasClass(compare);
      }else{
        return $(b).find('i').hasClass(compare) - $(a).find('i').hasClass(compare);
      }
    });

    ulSelector.append(liSelector);

    $(this).find('i:last-child').toggleClass('fa-caret-up').toggleClass('fa-caret-down');
  });

  $(document).on('click', '#sort-read', function(evt){
    evt.stopPropagation();
    var ulSelector = $("#notification-list-" + $(this).data('group') + " > .notifications");
    var liSelector = ulSelector.children('li#notification');
    var up = $(this).find('i:last-child').hasClass('fa-caret-up');

    liSelector.detach().sort(function(a, b){
      if(up){
        return $(a).data('read') - $(b).data('read');
      }else{
        return $(b).data('read') - $(a).data('read');
      }
    });

    ulSelector.append(liSelector);

    $(this).find('i:last-child').toggleClass('fa-caret-up').toggleClass('fa-caret-down');
  });
});
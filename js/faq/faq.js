function toggleVisibility(arr){
  $(arr.join(",")).toggle(100);
}

$(document).ready(function(){
  CKEDITOR.replace('answer');

  /******************************************************************************************************************************
  *******************************************************************************************************************************
                                                FAQ ITEM LISTENERS
  *******************************************************************************************************************************
  ******************************************************************************************************************************/
  $('[id=item-edit]').on('click', function(){
    var item = "faq-text-" + $(this).data('id');
    var faqSelector = "#faq-container-" + $(this).data('id');
    var faqFormSelector = "#faq-form-" + $(this).data('id');


    console.log(item, faqSelector, faqFormSelector);
    if(CKEDITOR.instances[item] === undefined)
      CKEDITOR.replace(item);
    toggleVisibility([faqSelector, faqFormSelector]);
  });

  $("[id=cancel-edit]").on('click', function(){
    var faqSelector = "#faq-container-" + $(this).data('id');
    var faqFormSelector = "#faq-form-" + $(this).data('id');
    toggleVisibility([faqSelector, faqFormSelector]);
  });

  $('#add-faq').on('click', function(){
    toggleVisibility(['#add-faq-form', '#add-faq', '#faq-search']);
  });

  $('#cancel-faq').on('click', function(){
    toggleVisibility(['#add-faq-form', '#add-faq', '#faq-search']);

    $('#title').val('');
    CKEDITOR.instances.answer.setData('');
  });

  /******************************************************************************************************************************
  *******************************************************************************************************************************
                                                FAQ FILE LISTENERS
  *******************************************************************************************************************************
  ******************************************************************************************************************************/

  $('[id=edit-file-desc], [id=cancel-file]').on('click', function(){
    var fileSelector = "#file-container-" + $(this).data('id');
    var fileFormSelector = "#file-form-" + $(this).data('id');

    toggleVisibility([fileSelector, fileFormSelector]);
  });
});
  /*
    This handles hierarchy checkboxes when click occur

    1. If the box is a parent (has the flag aria-parent)
      1.1 Click all the boxes bellow as well
      1.2 Check if the lower node is a parent as well and do 1.1 if true

    2. If the box is a child
      2.1 Click the parent box above
      2.2 Check if there is a root to the parents' box and check the brother boxes to determine what to do with the root box
  */
function checkboxHierarchy(elem){
  var selector = "#" + $(elem).attr('id');
  var brotherArray, childArray, subChildArray, i, j, checked;
  var filterArray = [];
  var markerArray = [];
  var jsonData, pointArray;
  
  if($(elem).attr('aria-parent') === 'true'){
  
    childArray = $("[data-id=" + $(elem).attr('id') + "]");
    for(i = 0; i < childArray.length; i++){
  
      $(childArray[i]).prop('checked', $(elem).prop('checked'));
      
      if($(childArray[i]).attr('aria-parent') === 'true'){  
  
        subChildArray = $("[data-id=" + $(childArray[i]).attr('id') + "]");
        for(j = 0; j < subChildArray.length; j++){
          $(subChildArray[j]).prop('checked', $(childArray[i]).prop('checked'));
        }
      }
    }
  }

  if($(elem).attr('aria-child') === 'true'){
    var parent = "#" + $(elem).data('id');
    
    childArray = $("[data-id=" + $(elem).data('id') + "]");
    checked = 0;
    
    for(i = 0; i < childArray.length; i++){
    
      if($(childArray[i]).is(':checked'))
        checked++;
    }

    $(parent).prop('indeterminate', checked < childArray.length && checked > 0);
    $(parent).prop('checked', checked === childArray.length);

    if($(parent).attr('aria-child') === 'true'){
    
      var rootBox = "#" + $(parent).data('id');
      brotherArray = $("[data-id=" + $(parent).data('id') + "]");
      checked = 0;

      for(i = 0; i < brotherArray.length; i++){
        
        if($(brotherArray[i]).is(':checked'))

          checked++;
      }

      $(rootBox).prop('indeterminate', checked < brotherArray.length && checked > 0);
      $(rootBox).prop('checked', checked === brotherArray.length);
    }
  }
}

$(document).ready(function () {
  var iconRed = L.icon({iconUrl: '../../imgs/pde_cvp_splitter_icon.png'});
  
  var mymap = L.map('map-container', {zoomControl: false}).setView([-8.9192338, 13.183256], 12);
  L.control.zoom({
    position:'bottomright'
  }).addTo(mymap);

  var marker = L.marker();

  var popup = L.popup();
  var html = "";
  var overlays = {};

  function populateBuilding(html, elem){
    var cols = 4; 
    var rows = 10;
    var random = Math.floor(Math.random() * 2500000) + 1;
    var i, j;
    
    html  = "<div class='popup-container'>";
    html +=   "<h3>Edifício exemplo:" + random + "</h3>";
    html +=    "<ul id='nav-container' class='nav nav-pills nav-justified'>";
    html +=      "<li role='presentation' class='active'><a data-id='building-container'>Edificio</a></li>";
    html +=      "<li role='presentation'><a data-id='building-equipments'>Equipamentos</a></li>";
    html +=      "<li role='presentation'><a data-id='building-attachments'>Anexos</a></li>";
    html +=    "</ul>";
    html +=    "<div id='building-container'>";
    html +=      "<table class='table table-bordered'>";
    html +=        "<thead>";
    html +=          "<tr>";
    html +=            "<th>Andar</th>";
    html +=            "<th>Fracção</th>";
    html +=            "<th></th>";
    html +=            "<th></th>";
    html +=          "</tr>";
    html +=        "</thead>";
    html +=        "<tbody>";
    
    for(i = 0; i < rows; i++){
      random = Math.floor(Math.random() * 2500000) + 1;
      html +=        "<tr>";
      html +=          "<td>" + i + "</td>";
      html +=          "<td>" + i + (rows-i) + "</td>";
      html +=          "<td><i class='fa fa-plus'></i></td>";
      html +=          "<td>" + random +"</td>";
      html +=        "</tr>";
    }
    html +=        "</tbody>";
    html +=      "</table>";
    html +=    "</div>";
    html +=    "<div id='building-equipments' hidden>";
    html +=      "<h3>Equipamentos</h3>";
    html +=    "</div>";
    html +=    "<div id='building-attachments' hidden>";
    html +=      "<h3>Anexos</h3>";
    html +=    "</div>";
    return html;
  }

  L.tileLayer('https://api.mapbox.com/styles/v1/ramongr/ciq9k6bpc009idtnpdslspvfp/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmFtb25nciIsImEiOiJjaXE5azFnY24wMDVqaHluc3RqcHVjbTVrIn0.CBQYRfMKoWXOWbdmjtHPcw', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'map-container',
    accessToken: 'pk.eyJ1IjoicmFtb25nciIsImEiOiJjaXE5azFnY24wMDVqaHluc3RqcHVjbTVrIn0.CBQYRfMKoWXOWbdmjtHPcw'
  }).addTo(mymap);

  var drawnItems = new L.FeatureGroup();
  mymap.addLayer(drawnItems);

  var drawControl = new L.Control.Draw({
    edit: {
      featureGroup: drawnItems
    },
    draw: {
      rectangle: false,
      circle: false
    },
    position: 'topright'
  });
  mymap.addControl(drawControl);

  mymap.on('draw:created', function(evt){
    var type = evt.layerType,
      layer = evt.layer;

    $('#mapInfo').modal('show');
    drawnItems.addLayer(layer);
  });

  $('[id=information]').on('click', function(evt){
    evt.stopPropagation();

    $('#mapInfo').modal('show');
  });

  $('li').on('click', function(){
    latlng = new L.LatLng($(this).data('latitude'), $(this).data('longitude'));
    marker.setLatLng(latlng).addTo(mymap);
    mymap.setView([$(this).data('latitude'), $(this).data('longitude')], 17, {animate: true});
    $('li.in-sight').removeClass('in-sight');
    $(this).addClass('in-sight');
  });


  // mymap.on('click', function(e){
  //   console.log(e.latlng.lat, e.latlng.lng);
  // });


  $(document).on('click', '#nav-container > li > a', function(){
    var currentSelector = "#" + $(this).data('id');
    var previousSelector = "#" + $('#nav-container > li.active > a').data('id');
    
    $('#nav-container > li.active').removeClass('active');
    $(this).parent().addClass('active');
    $(currentSelector).show();
    $(previousSelector).hide();
  });
  
  $('#slide-submenu').on('click',function() {             
    $(this).closest('#sidemenu-container').fadeOut('slide',function(){
      $('.mini-submenu').fadeIn();  
    });      
  });

  $('.mini-submenu').on('click',function(){   
    $(this).next('#sidemenu-container').toggle('slide');
    $('.mini-submenu').hide();
  });

  $(document).on('click', '[id=collapsable]', function(evt){
    evt.preventDefault();
    console.log($(this));
    var selector = "#" + $(this).data('selector');
    $(selector).toggle(125);
    $(this).find('i:first').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
  });

  $(document).on('click', 'input[type=checkbox][aria-loaded=false]', function(){
    checkboxHierarchy(this);
    var index = $(this).data('file'),
      levels = 0,
      path = [
        "..",
        "json",
        index
      ],
      pointData;

    index = index.split(".")[0];
    $.ajax({
      url: path.join("/"),
      contentType: 'json',
      success: function(data){
        pointData = data[index];
      }
    })
    .then(function(){
      var markerArray = [];
      for(var firstLevel in pointData){
        if(typeof pointData[firstLevel] === 'object'){
          for(var secondLevel in pointData[firstLevel]){
            if(pointData[firstLevel][secondLevel].length !== undefined){
              for(var i = 0; i < pointData[firstLevel][secondLevel].length; i++){
                marker = L.marker(
                  [pointData[firstLevel][secondLevel][i].latitude, 
                  pointData[firstLevel][secondLevel][i].longitude],
                  {
                    icon: iconRed,
                    title: index
                  }
                );

                markerArray.push(marker);
              }
            }
          }
        }else{
          console.log(pointData[firstLevel]);
        }
      }

      overlays[index] = L.layerGroup(markerArray);

      L.control.layers(null, overlays).addTo(mymap);
    });
  });
});

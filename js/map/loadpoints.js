$(document).ready(function(){

  /*
    Load points for the map
  */
  var iconRed = "../../imgs/red_point.jpg";
  var data;
  var overlays = {};

  $(document).on('click', "[aria-loaded=false]", function(){
    var index = $(this).data('file'),
      levels = 0,
      path = [
        "..",
        "json",
        index
      ],
      pointData;

    index = index.split(".")[0];
    $.ajax({
      url: path.join("/"),
      contentType: 'json',
      success: function(data){
        pointData = data[index];
      }
    })
    .then(function(){
      console.log(pointData);
      overlays[index] = [];
      for(var firstLevel in pointData){
        if(typeof pointData[firstLevel] === 'object'){
          for(var secondLevel in pointData[firstLevel]){
            if(pointData[firstLevel][secondLevel].length !== undefined){
              for(var i = 0; i < pointData[firstLevel][secondLevel].length; i++){
                marker = L.marker(
                  [pointData[firstLevel][secondLevel][i].latitude, 
                  pointData[firstLevel][secondLevel][i].longitude],
                  {
                    icon: iconRed,
                    title: index
                  }
                );

                overlays[index].push(marker);
              }
            }
          }
        }else{
          console.log(pointData[firstLevel]);
        }
      }

      L.control.layers(null, overlays);
    });
  });
});